import Column from './charts/Highcharts/Column';
import Line from './charts/Highcharts/Line';
import Area from './charts/Highcharts/Area';
import Scatter from './charts/Highcharts/Scatter';
import Funnel from './charts/Highcharts/Funnel';
import Pie from './charts/Highcharts/Pie';
import Bullet from './charts/Highcharts/Bullet';
import Radar from './charts/Highcharts/Radar';
import Gauge from './charts/Highcharts/Gauge';
import Bubble from './charts/Highcharts/Bubble';
import Waterfall from './charts/Highcharts/Waterfall';
import Treemap from './charts/Highcharts/Treemap';
import Heatmap from './charts/Highcharts/Heatmap';

let charts = {
  column: Column,
  line: Line,
  area: Area,
  scatter: Scatter,
  funnel: Funnel,
  pie: Pie,
  donut: Pie,
  bullet: Bullet,
  radar: Radar,
  gauge: Gauge,
  bubble: Bubble,
  waterfall: Waterfall,
  treemap: Treemap,
  heatmap: Heatmap
};

export default function (options) {

  return charts[options.type](options);

}
