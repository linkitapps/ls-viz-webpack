import Highcharts from 'highcharts';
import heatmap from 'highcharts/modules/heatmap';

heatmap(Highcharts);

export default function Heatmap(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'heatmap'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
