import Highcharts from 'highcharts';

export default function Scatter(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'scatter'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
