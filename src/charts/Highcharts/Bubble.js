import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';

highchartsMore(Highcharts);

export default function Bubble(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'bubble'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
