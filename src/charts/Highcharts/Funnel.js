import Highcharts from 'highcharts';
import funnel from 'highcharts/modules/funnel';

funnel(Highcharts);

export default function Funnel(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'funnel'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
