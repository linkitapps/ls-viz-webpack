import Highcharts from 'highcharts';

export default function Pie(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'pie'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
