import Highcharts from 'highcharts';

export default function Area(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'area'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
