import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';

highchartsMore(Highcharts);

export default function Waterfall(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'waterfall'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
