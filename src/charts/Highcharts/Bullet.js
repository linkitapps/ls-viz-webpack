import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';

highchartsMore(Highcharts);

Highcharts.Renderer.prototype.symbols.line = function (x, y, width, height) {
  return ['M', x, y + width / 2, 'L', x + height, y + width / 2];
};

export default function Bullet(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
