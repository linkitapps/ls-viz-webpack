import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';

highchartsMore(Highcharts);

export default function Radar(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      polar: true,
      type: 'line'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
