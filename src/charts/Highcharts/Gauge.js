import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';

highchartsMore(Highcharts);

export default function Gauge(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'gauge'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
