import Highcharts from 'highcharts';

export default function Line(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'line'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
