import Highcharts from 'highcharts';

export default function Column(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'column'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
