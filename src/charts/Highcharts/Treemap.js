import Highcharts from 'highcharts';
import heatmap from 'highcharts/modules/heatmap';
import treemap from 'highcharts/modules/treemap';

heatmap(Highcharts);
treemap(Highcharts);

export default function Treemap(options) {

  let chartOptions = Highcharts.merge(options.chartOptions, {
    chart: {
      type: 'treemap'
    },
    credits: {
      enabled: false
    }
  });

  Highcharts.chart(options.container, chartOptions);

}
